#!/bin/sh
for distro in ubuntu arch arch-root-nogcc arch-manual arch-root lostfiles arch-after-root ; do
  docker build -t pypy3bug:$distro -f Dockerfile-$distro .
  docker run --rm -v "$(pwd)":/code pypy3bug:$distro
done
